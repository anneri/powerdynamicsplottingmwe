using PkgTemplates
using Pkg

# activate local environment
Pkg.activate("")


# put in your own credentials first
t = Template(;
    authors="Anne Riegler <anneri@pik-potsdam.de>",
    user="anneri",
    host="gitlab.com",
    plugins=[
        GitLabCI(),
        Documenter{GitLabCI}(),
        ProjectFile(; version=v"1.0.0-DEV"),
        #Readme(;
        #file="~/.julia/packages/PkgTemplates.jl/PkgTemplates.jl/templates/README.md",
        #destination="README.md",
        #inline_badges=false,
        #),
    ],
)

# choose a name and generate the package
t("PowerDynamicsPlottingMWE")

# install some packages, for example:

# in terminal:
# ] add PowerDynamics


# or: executing:

using Pkg
Pkg.add("PowerDynamics")

# then build the Manifest.toml
Pkg.instantiate()